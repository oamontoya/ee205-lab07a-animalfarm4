#include <string>
#include <iostream>

#include "utility.hpp"

using namespace std;


namespace animalfarm {
   
   std::string generateRandomName() {
      int length = 5;
      char outputName[length];

      outputName[0] = randomUppercaseLetter();

      for (int i = 1; i < length; i++)
         outputName[i] = randomLowercaseLetter();

      return outputName;
   }

   enum animalfarm::Color generateRandomColor() {
      return animalfarm::Color(rand() % 6);
   }

   enum animalfarm::Gender generateRandomGender() {
      return animalfarm::Gender(rand() % 2);
   }

   bool generateRandomBool(){
      return bool( rand() % 2 );
   }

   float generateRandomWeight() {
      return 1.35;
   }

   float generateRandomTemp() {
      return 3.7;
   }

   std::string generateRandomLocation() {
      return "Sample Location";
   }

   char randomUppercaseLetter() {
      return ( rand() % 26 ) + 65;
   }

   char randomLowercaseLetter() {
      return ( rand() % 26 ) + 97;
   }
}


//int main() {
   //srand(time(NULL));

   //cout << "Testing utility functions" << endl;
   //cout << "Random Uppercase letter: " << animalfarm::randomUppercaseLetter() << endl;
   //cout << "Random Lowercase letter: " << animalfarm::randomLowercaseLetter() << endl;
   //cout << "Random Name: " << animalfarm::generateRandomName() << endl;
   //cout << "Random Color: " << animalfarm::generateRandomColor() << endl;
   //cout << "Random Gender: " << animalfarm::generateRandomGender() << endl;
  
   //for( int i = 0; i < 25; i++){
      
      //animalfarm::Animal* newAnimal = animalfarm::AnimalFactory::getRandomAnimal();
      //newAnimal->printInfo();
      //cout << newAnimal->speak() << endl;
   //}
   //return 0;
//}

