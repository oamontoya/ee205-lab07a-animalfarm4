///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file testharness.cpp
/// @version 1.0
///
/// A test program used for testing specific units of the overall project
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   28_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>
#include <cstring>
#include "node.hpp"
#include "list.hpp"


using namespace std;

void printListStats(SingleLinkedList);
string addrToName(Node*);


//create a list
SingleLinkedList list;
//create some test nodes
Node node1, node2, node3, node4, node5;

int main() {
   //print some information about the (empty) list
   printListStats(list);
   
   //add some nodes to the list
   cout << "Added node 1 to list" << endl;
   list.push_front(&node1);      //n1
   printListStats(list);
   cout << "Added node 2 to list" << endl;
   list.push_front(&node2);      //n2 n1
   printListStats(list);
   cout << "Added node 3 to list" << endl;
   list.push_front(&node3);      //n3 n2 n1
   //print some information about the (full) list
   printListStats(list);
   //the list should be as follows:
   //node3 -> node2 -> node1


   //remove the first node in the list
   cout << "Pop first node" << endl;
   list.pop_front();             //n2 n1
   printListStats(list);
   //the list should now be:
   //node2 -> node1
   
   //confirm that the first node is now node2
   cout << "Check that the first node is now node 2: ";
   if(list.get_first() == &node2)
      cout << "yes" << endl;

   //confirm that node2.next points to node1
   cout << "Check that the node after node 2 is node 1: ";
   if(list.get_next(list.get_first()) == &node1)
      cout << "yes" << endl;
   cout << endl;

   cout << "swap node 1 (at end of list)" << endl;
   list.swap(&node1);         //n2 n1 (nothing should happen)
   printListStats(list);

   cout << "swap node 2 with node 1" << endl;
   list.swap(&node2);         //n1 n2
   printListStats(list);
   cout << "Pop all nodes" << endl;
   list.pop_front();          //n2
   list.pop_front();          //empty
   list.pop_front();          //empty (should not crash)
   printListStats(list);

   cout << "Added node 1 to list" << endl;
   list.push_front(&node1);      //n1
   printListStats(list);
   cout << "Added node 2 to list" << endl;
   list.push_front(&node2);      //n2 n1
   printListStats(list);
   cout << "Added node 3 to list" << endl;
   list.push_front(&node3);      //n3 n2 n1
   printListStats(list);
   cout << "Added node 4 to list" << endl;
   list.push_front(&node4);      //n4 n3 n2 n1
   printListStats(list);
   cout << "Added node 5 to list" << endl;
   list.push_front(&node5);      //n5 n4 n3 n2 n1
   printListStats(list);

   cout << "swap node 5 and 4" << endl;
   list.swap(&node5);      //n4 n5 n3 n2 n1
   printListStats(list);
   cout << "swap node 5 and 3" << endl;
   list.swap(&node5);      //n4 n3 n5 n2 n1
   printListStats(list);
   cout << "swap node 5 and 2" << endl;
   list.swap(&node5);      //n4 n3 n2 n5 n1
   printListStats(list);
   cout << "swap node 5 and 1" << endl;
   list.swap(&node5);      //n4 n3 n2 n1 n5
   printListStats(list);
   cout << "swap node 5 and nothing (end of list)" << endl;
   list.swap(&node5);      //n4 n3 n2 n1 n5 (should not crash)
   printListStats(list);

   list.bubbleSort();
   printListStats(list);


}


void printListStats(SingleLinkedList inputList) {
   int count = 0;
   Node* currentNode = inputList.get_first();
   cout << addrToName(currentNode);
   while((currentNode != nullptr) && (count < 10)) {
      cout << "->" << addrToName(inputList.get_next(currentNode));
      currentNode = inputList.get_next(currentNode);
      count++;
   }
   cout << endl;
   cout << "Current List Stats:" << endl;
   cout << "Size: " << inputList.size() << endl;
   cout << "Is empty?: " << boolalpha << inputList.empty() << endl;
   cout << endl;
}

string addrToName(Node* inputNode) {
   if(inputNode == &node1)
      return "node 1";
   else if(inputNode == &node2)
      return "node 2";
   else if(inputNode == &node3)
      return "node 3";
   else if(inputNode == &node4)
      return "node 4";
   else if(inputNode == &node5)
      return "node 5";
   else if(inputNode == nullptr)
      return "nullptr";
   else
      return "unknown";
}
