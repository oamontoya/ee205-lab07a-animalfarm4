###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 07a - Animal Farm 4
#
# @file    Makefile
# @version 1.0
#
# @author Osiel Montoya <montoyao@hawaii.edu>
# @brief  Lab 07a - Animal Farm 4 - EE 205 - Spr 2021
# @date   28_MAR_2021
###############################################################################

all: main

SRC= list.cpp node.cpp utility.cpp animal.cpp animalfactory.cpp mammal.cpp cat.cpp dog.cpp fish.cpp nunu.cpp aku.cpp bird.cpp palila.cpp nene.cpp
OBJ= $(SRC:.cpp=.o)
HDR= $(SRC:.cpp=.h)

testharness.o: testharness.cpp
	g++ -c testharness.cpp

main.o: main.cpp
	g++ -c main.cpp

main: main.cpp  main.o $(OBJ)
	g++ -o $@ main.o $(OBJ)

test: testharness.cpp testharness.o $(OBJ)
	g++ -o $@ testharness.o $(OBJ)

clean:
	rm -f *.o main test
