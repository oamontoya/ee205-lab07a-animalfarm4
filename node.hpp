///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4 
///
/// @file node.hpp
/// @version 1.0
///
/// An element in the custom SingleLinkedList class
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   28_MAR_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once


class Node {
   friend class SingleLinkedList;
   protected:
      Node* next = nullptr;
   public:
      virtual bool operator>(const Node& r);
};
