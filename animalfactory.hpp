///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4 
///
/// @file animalfactory.hpp
/// @version 1.0
///
/// Randomly creates new animals
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   28_MAR_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include "utility.hpp"

using namespace std;

namespace animalfarm {

   class AnimalFactory {
      public:
         static Animal* getRandomAnimal();
   };
}

