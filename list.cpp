///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
/// A custom implementation of a single linked list
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   28_MAR_2021
///////////////////////////////////////////////////////////////////////////////
#include "list.hpp"

//empty():determines whether the list is empty or not
//input: none
//output: none
//return: boolean indicating whether the first node in the list is nullptr, indicating an empty list
const bool SingleLinkedList::empty() const {
      return head == nullptr;
}

//push_front(): takes the node pointer passed in and sets it as the new head of the list
//input: Node* pointer newNode
//output: none
//return: none (void)
void SingleLinkedList::push_front(Node* newNode) {
   newNode->next = head;
   head = newNode;
}

//pop_front(): removes the first node in the list 
//input: none
//output: none
//return: the node that was removed, or nullptr if the list was empty
Node* SingleLinkedList::pop_front() {
   Node* deletedNode = head;
   if(deletedNode != nullptr) {
      head = deletedNode->next;
   }
   return deletedNode;
}

//get_first(): gets the head of the list
//input: none
//output: none
//return: the head of the list, the first node
Node* SingleLinkedList::get_first() const {
   return head;
}

//get_next(): gets the next node from the node pointer passed in
//input: a node pointer
//output: none
//return: the next node pointer in the list from the input node pointer,
//or nullptr if the input node was the last in the list
Node* SingleLinkedList::get_next(const Node* currentNode) const {
   return currentNode->next;
}

//size(): measures the number of nodes in the list
//input: none
//output: none
//return: the number of nodes in the list
unsigned int SingleLinkedList::size() const {
   unsigned int size = 0;
   Node* currentNode = head;
   while(currentNode != nullptr) {
      size++;
      currentNode = currentNode->next;
   }
   return size;
}

//swap(): swap two adjacent nodes
void SingleLinkedList::swap(Node* node1) {
   //special case: the list is empty
   if(size() == 0)
      return;
   //special case: the list has only one node
   else if(size() == 1)
      return;
   //general case: the list has at least two nodes
   else {
      //special case: the input node is the last node in the list
      if(node1->next == nullptr) {
         return;
      }
      //special case: the input node is the first node in the list
      else if(node1 == head) {
         Node* nextNode = node1->next->next;
         head = node1->next;
         node1->next->next = node1;
         node1->next = nextNode;
      }
      //general case: the node to be swapped is at least one node away from the end of the list
      else {
         Node* prevIterator = get_first();
         Node* nextNode = node1->next->next;

         while(prevIterator->next != node1) {
            prevIterator = prevIterator->next;
         }
         
         prevIterator->next = node1->next;
         node1->next->next = node1;
         node1->next = nextNode;
      }
   }
}

void SingleLinkedList::bubbleSort() {
   bool isSorted = true;
   Node* iterator = get_first();
   
   //special case: a list of size less than one is already sorted
   if(size() == 1 || size() == 0) {
      return;
   }
   //general case: a list of size greater than one
   else {
      //iterate through the list
      while(iterator != nullptr) {
         //compare the nodes
         if(*iterator > *iterator->next) {
            swap(iterator);
            isSorted = false;
         }
         else {
            iterator = iterator->next;
         }

         if(iterator->next == nullptr) {
            if(isSorted == true) {
               return;
            }
            else {
               iterator = get_first();
               isSorted = true;
            }
         }
      }
   }
}
