///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file main.cpp
/// @version 1.0
///
/// Main program
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   29_MAR_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <list>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "animalfactory.hpp"
#include "utility.hpp"
#include "node.hpp"
#include "list.hpp"

using namespace std;
using namespace animalfarm;

int main() {
	cout << "Welcome to Animal Farm 4" << endl;

   //Animal List
   
   SingleLinkedList animalList;

   //create 25 random animals, pushing each to the front of the list
   for( auto i = 0 ; i < 25 ; i++ ) {
      animalList.push_front( AnimalFactory::getRandomAnimal());
   }

   //print out some information about the list
   cout << endl;
   cout << "List of Animals" << endl;
   cout << " Is it empty: " << boolalpha << animalList.empty() << endl;
   cout << " Number of elements: " << animalList.size() << endl;

   //iterate through the entire list, making each animal speak
   for( auto animal = animalList.get_first(); 
         animal != nullptr;
         animal = animalList.get_next( animal )) {
      cout << ((Animal*)animal)->species << endl;
   }

   animalList.bubbleSort();

   cout << endl;

   //iterate through the entire list, making each animal speak
   for( auto animal = animalList.get_first(); 
         animal != nullptr;
         animal = animalList.get_next( animal )) {
      cout << ((Animal*)animal)->species << endl;
   }

   //remove the first item in the list, repeating until the list is empty
   while( !animalList.empty() ) {
      Animal* animal = (Animal*) animalList.pop_front();

      delete animal;
   }

   return 0;
}
