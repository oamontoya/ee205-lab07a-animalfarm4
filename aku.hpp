///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file aku.hpp
/// @version 1.0
///
/// Exports data about all fish
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   10_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>

#include "fish.hpp"

using namespace std;

namespace animalfarm {
   class Aku : public Fish {
   public:
	   Aku( float newWeight, enum Color newColor, enum Gender newGender, float newFavoriteTemp );
     
      float weight;
		
	   void printInfo();
   };
}
